import os;
from cx_Freeze import setup, Executable

base = None


executables = [Executable("Simple Calculator by Python.py", base=base)]

packages = ["idna"]
options = {
    'build_exe': {

        'packages':packages,
    },

}

os.environ['TCL_LIBRARY'] = "C:\\Users\\Pranta\\AppData\\Local\\Programs\\Python\\Python36-32\\tcl\\tcl8.6"
os.environ['TK_LIBRARY'] = "C:\\Users\\Pranta\\AppData\\Local\\Programs\\Python\\Python36-32\\tcl\\tk8.6"

setup(
    name = "Pranta",
    options = options,
    version = "3.1",
    description = 'None',
    executables = executables
)
